#!/usr/bin/env python3
import cgi

form = cgi.FieldStorage()

first_name = form['first_name'].value
last_name = form['last_name'].value
parking_id = form['parkingId'].value

print("Content-Type: x-text/html-fragment\n\n")

htmlFormat = """
  <p>Booking parking space with id={parking_id}</p>
  <p>{first_name} {last_name}</p>
"""

print(htmlFormat.format(**locals()))