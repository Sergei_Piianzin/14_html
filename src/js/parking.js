window.onload = function() {
	application.init();
}

var application = {};

application.init = function() {
	application.view = new application.view();
	application.controller = new application.controller(application.model, application.view);
}

application.model = {
	buttons : {
		ids : ['button1', 'button2', 'button3'],
		items : [],
	},
	parking : {
		ids : [],
		items : [],
	},
	popup : { 
		ids : ['form1'],
		items : [],
		parkingId : "",
		call : function(popupId) {
			var msg = $('#' + popupId).serialize() + '&parkingId=' + application.model.popup.parkingId;
			$.ajax({
				data: msg,
				url: 'cgi-bin/form_answer.py',
				success: function(result) {
					application.model.popup.showAnswer(result);
				},
				error: function(xhr, str){
					alert('Возникла ошибка: ' + xhr.responseCode);
				}
			});
		},
		showAnswer : function(data) {
			$("#results").append(data);
		}
	}
}

application.view = function() {
	var view = this;

	this.show = function(id){
		var elem = document.getElementById(id);
		elem.style.display = 'block';
	};

	this.hide = function(id) {
		var elem = document.getElementById(id);
		elem.style.display = 'none';
	}

	this.toggleClass = function (object, clazz) {
		object.classList.toggle(clazz);
	}

	this.getElementsByIds = function(ids) {
		var elems = [];
		ids.forEach(function(id, i) {
			elems.push(document.getElementById(id));
		});
		return elems;
	};

	this.showContent = function(id) {
		var elems = document.getElementsByClassName('content');
		[].forEach.call(elems, function(item) {
			if (item.id !== id) {
				view.hide(item.id);
			}
		});
		view.show(id);
	};

	this.generateParking = function(count) {
		var elem = document.getElementById('parkingBlock');
		var ids = [];
		for (var i = 0; i < count; i++) {
			ids[i] =  'parking' + i;
			elem.innerHTML += '<div class="parking" id=' + ids[i] +'><img src="img/car.png"><p>' + ids[i] +'</p></div>';
		};
		application.model.parking.ids = ids;
	}

	this.generateParking(7);
}

application.controller = function(model, view){
	this.initTabs = function() {
		model.buttons.items = view.getElementsByIds(model.buttons.ids);
	};

	this.initParking = function() {
		model.parking.items = view.getElementsByIds(model.parking.ids);
	};

	this.initPopup = function() {
		model.popup.items = view.getElementsByIds(model.popup.ids);
	}

	this.bindTabs = function() {
		model.buttons.items.forEach(function(button) {
			button.addEventListener("mouseover", function () {view.toggleClass(button, 'pushed')});
			button.addEventListener("mouseout", function () {view.toggleClass(button, 'pushed')});
		});
	};

	this.bindParking = function() {
		model.parking.items.forEach(function(space) {
			space.addEventListener("click", function() {
				view.show('popup');
				model.popup.parkingId = space.id;
			});
			space.addEventListener("mouseover", function () {view.toggleClass(space, 'pushedIcon')});
			space.addEventListener("mouseout", function () {view.toggleClass(space, 'pushedIcon')});
		});
	};

	this.bindContent = function() {
		var buttons = model.buttons.items;
		buttons[0].addEventListener("click", function () {view.showContent("parkingBlock");});
		buttons[1].addEventListener("click", function () {view.showContent("employeeBlock");});
		buttons[2].addEventListener("click", function () {view.showContent("letterBlock");});
	};

	this.bindPopup = function() {
		var popupId = model.popup.ids[0];
		var form = model.popup.items[0];
		form.addEventListener("submit", function () {model.popup.call(popupId)});

		var cross = document.getElementsByClassName('cross');
		cross[0].addEventListener("click", function () {view.hide('popup')});
	};

	this.init = function () {
		this.initTabs();
		this.initParking();
		this.initPopup();
		this.bindTabs();
		this.bindContent();
		this.bindParking();
		this.bindPopup();
		view.show(document.getElementById('parkingBlock').id);
	};

	this.init();
} 